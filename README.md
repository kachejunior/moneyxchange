# Belatrix Test
Desarrollador: Carlos Javier Mendoza Hernandez.

Correo: cmendoza5806@yahoo.com


Prueba de front-end developer de Toolbox.
Tecnología:  NodeJS.

## Descripción:

Un equipo de entrepreneurs está diseñando el modelo de negocio de su startup http://moneyxchange.io
El principal problema que tienen en este momento es el desarrollo de su landing page con un formulario que le permita a sus usuarios cotizar el tipo de cambio ofrecido por ellos.
La startup está localizada en Estados Unidos, y por el momento solo ofrecen cambio de divisas para Europa, es decir el tipo de cambio es solo entre Euros y Dólares americanos. Ten en cuenta que ellos piensan expandirse y añadir más divisas progresivamente.
Los requerimientos como una primera fase que se debe cubrir el landing page es el siguiente:
1. Cotizar desde Dólares americanos a Euros.
Criterio de aceptación: Si se ingresa un valor numérico (con hasta cuatro decimales inclusive) en la caja de texto indicada, se deberá obtener el precio del Euro mediante un servicio rest y mostrar el valor convertido a Euros en una caja de texto no editable.
El equipo de UX ha propuesto un wireframe para el landing page, indicando los dos inputs, el de dólares editable y el de euros no editable. Adicionalmente se propone un botón para realizar el cálculo, en principio se pensó usar el evento keyup del input, pero resultó muy compleja la interacción en los user tests. Es muy importante el formato de los inputs, se deben reflejar los valores con formato moneda con su respectivo símbolo, punto para separar los decimales y coma para separar los miles. En cuanto a responsive design, el equipo de UX solo pide que al verse en dispositivos móviles, los elementos input ocupen el 100% de la pantalla, es decir que sean fluidos. El wireframe se encuentra adjunto en el apartado “Wireframe”
La tecnología de backend que piensan usar es muy secreta y ellos sólo brindarán acceso a un API Rest desarrollada por sus propios ingenieros.
Para el requerimiento 1: Cotizar desde Dólares americanos a Euros, se pide usar el siguiente endpoint: ​http://api.fixer.io/latest?base=USD&symbols=EUR
Tomar en cuenta que el precio de las divisas suele cambiar cada 10 minutos, así que sería deseable pero no obligatorio implementar un mecanismo que permita persistir temporalmente el precio obtenido por al menos 10 minutos, pasado ese tiempo, se debería llamar nuevamente al servicio rest para obtener el nuevo precio.
El equipo está muy apurado con los tiempos y solo pide hacer un único unit test probando la funcionalidad de la cotización y es de manera opcional ya que no ha tenido tiempo de implementar su pipeline de integración y delivery continuo.
Para presentar el trabajo terminado, se sugiere subir el proyecto a un repositorio público de los proveedores mencionados en el apartado “Pre requisitos”, luego de subir el proyecto a la nube, dar aviso al reclutador.


## Para instalar

1. Clonar el proyecto en https://github.com/kachejunior/prueba-belatrix

2. Ejecutar en consola `npm install` que instalará todas las dependencias que necesitará el proyecto, se encuentran en el archivo package.json

3. Ejecutar `npm start` o `ng serve` y el servicio se levantara para desarrollo en http://localhost:4200

4. Ejecutar `ng build --prod` para copilar una carpeta en producción

5. Para ejecutar test usar el comando `ng test` y se ejecutara la prueba unitaria en http://localhost:9876
 
