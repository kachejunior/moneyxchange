import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavComponent } from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';

 
 
@NgModule({
  imports: [
    CommonModule // we use ngFor
  ],
  exports: [NavComponent, FooterComponent],
  declarations: [NavComponent, FooterComponent],
  providers: [
  ]
})
export class CoreModule { }