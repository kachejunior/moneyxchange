import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgxLoadingModule } from 'ngx-loading';

import { CurrencyService } from './../shared/currency.service';
import { CurrencyApi } from './../shared/currency.model';

import { ConverterComponent } from './converter.component';

describe('ConverterComponent', () => {

  const formBuilder: FormBuilder = new FormBuilder();
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, CommonModule, HttpClientModule, CurrencyMaskModule, NgxLoadingModule],
      declarations: [
        ConverterComponent
      ],
      providers: [
        CurrencyService,
        { provide: FormBuilder, useValue: formBuilder }
      ]
    }).compileComponents();
  }));


  it('should create the app', () => {
    const fixture = TestBed.createComponent(ConverterComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('form invalid when empty', () => {
    const fixture = TestBed.createComponent(ConverterComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.form.invalid).toBeTruthy();
  });

  it('submitting a converter', async(() => {
    const fixture = TestBed.createComponent(ConverterComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.form.valid).toBeFalsy();
    app.form.controls['dollar'].setValue("10");
    app.send();
    fixture.detectChanges();
    console.log(app.form.getRawValue());
    app.loading = false;
    expect(parseFloat(app.form.getRawValue().euro)).toBeGreaterThanOrEqual(8);


  }));
});
