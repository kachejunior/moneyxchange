import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CurrencyService } from './../shared/currency.service';
import { CurrencyApi } from './../shared/currency.model';


@Component({
  selector: 'mxc-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.scss'],
})
export class ConverterComponent implements OnInit {

  form: FormGroup;
  loading: boolean = false;
  control: boolean = false;


  ngOnInit() {
    interval(360000).subscribe(() => {
      console.log('Hola')
      if (this.control) {
        console.log("ejecutando...");
        this.send();
      }
    });
  }

  constructor(private currencyService: CurrencyService) {
    this.form = new FormGroup({
      dollar: new FormControl('', [Validators.required, Validators.min(0)]),
      euro: new FormControl('', [])
    })
  }

  send() {
    this.loading = true;
    this.currencyService.getCurrency(
      (response: any, err: any) => {
        if (err) {
          console.log(err);
        } else {
          let date: CurrencyApi = response;
          this.form.patchValue({
            euro: parseFloat(this.form.value.dollar) / date.rates.USD
          });

          this.control = true;
        }
        this.loading = false;
      }
    );
  }

  detect(){
    this.control = false;
  }


}