export class CurrencyApi {
    success: boolean;
    timestamp: number;
    base: string;
    date: string;
    rates: {
        USD: number
    };
}