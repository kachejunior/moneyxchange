import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CurrencyService } from './currency.service';


@NgModule({
    imports: [CommonModule, FormsModule, HttpClientModule],
    declarations: [

    ],
    providers: [CurrencyService],
    exports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
    ]
})
export class SharedModule { }