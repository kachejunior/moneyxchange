import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CurrencyService {
    constructor(private httpClient: HttpClient) { }

    getCurrency(callback: any) {
        this.httpClient.get('http://data.fixer.io/api/latest?access_key=e80651a7442b45e7b8e447811ae3d902&symbols=USD')
        .subscribe(data => {
            return callback(data, null);
        }, error => {
            return callback(null, error, null)
        });
    }
}